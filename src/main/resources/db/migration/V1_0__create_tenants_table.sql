CREATE TABLE tenants (
  id        BIGINT NOT NULL AUTO_INCREMENT,
  join_date DATETIME,
  name      VARCHAR(255),
  PRIMARY KEY (id)
);