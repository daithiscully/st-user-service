INSERT INTO tenants (`id`, `join_date`, `name`)
VALUES ('1', CURRENT_TIMESTAMP(), 'AIB');

INSERT INTO users (`id`, `first_name`, `join_date`, `last_name`, `password`, `username`, `tenant_id`)
VALUES ('1', 'David', CURRENT_TIMESTAMP(), 'Scully', 'Password1', 'MrNice', '1');