CREATE TABLE users (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(255),
  join_date  DATETIME,
  last_name  VARCHAR(255),
  password   VARCHAR(255),
  username   VARCHAR(255),
  tenant_id  BIGINT,
  PRIMARY KEY (id)
);

ALTER TABLE users
  ADD CONSTRAINT FK21hn1a5ja1tve7ae02fnn4cld FOREIGN KEY (tenant_id) REFERENCES tenants (id);