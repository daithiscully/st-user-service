package com.scully.userservice.domain.users.exceptions;

import com.scully.userservice.domain.users.User;

public class IncorrectPasswordException extends RuntimeException {

  private User user;

  public IncorrectPasswordException() {
  }

  public IncorrectPasswordException(String message, User user) {
    super(message);
    this.user = user;
  }
}
