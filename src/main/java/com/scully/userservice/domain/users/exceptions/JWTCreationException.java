package com.scully.userservice.domain.users.exceptions;

import com.scully.userservice.domain.users.User;

public class JWTCreationException extends RuntimeException {

  private User user;

  public JWTCreationException() {
  }

  public JWTCreationException(User user, String message) {
    super(message);
    this.user = user;
  }

}


