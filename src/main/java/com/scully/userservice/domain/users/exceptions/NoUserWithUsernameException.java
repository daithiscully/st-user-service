package com.scully.userservice.domain.users.exceptions;

public class NoUserWithUsernameException extends RuntimeException {

  public NoUserWithUsernameException() {
  }

  public NoUserWithUsernameException(String message) {
    super(message);
  }
}
