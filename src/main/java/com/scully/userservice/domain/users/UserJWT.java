package com.scully.userservice.domain.users;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.scully.userservice.domain.users.exceptions.JWTCreationException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import lombok.Data;

@Data
public class UserJWT {

  private static final String JWT_SECRET = "HArvf3z>&5JUDsxZHS~(S*r6ZB7?8Q^N?(hT)up4v/-C<]jsC";

  private final User user;
  private final String token;

  public UserJWT(User user) {
    this.user = user;
    this.token = createTokenForUser(user);
  }

  private String createTokenForUser(User user) {
    // Set the expire time to 15 minutes from creation
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.MINUTE, +15);
    try {
      String token = JWT.create()
          .withExpiresAt(cal.getTime())
          .withClaim("username", user.getUsername())
          .withClaim("id", Double.valueOf(user.getId()))
          .withIssuer("auth0")
          .sign(Algorithm.HMAC256(JWT_SECRET));
      System.out.println("Token created!");
      return token;
    } catch (JWTCreationException | UnsupportedEncodingException exception) {
      throw new JWTCreationException(user, exception.getMessage());
    }
  }
}
