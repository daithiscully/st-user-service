package com.scully.userservice.domain.users;

import com.scully.userservice.domain.users.exceptions.IncorrectPasswordException;
import com.scully.userservice.domain.users.exceptions.NoUserWithUsernameException;
import java.util.Date;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  private final UserRepository userRepository;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public User saveNewUser(User user) {
    user.setJoinDate(new Date());
    return userRepository.save(user);
  }

  public User getUserById(Long id) {
    return userRepository.findById(id);
  }

  public UserJWT authenticateUser(User user) {
    User userFromDb = userRepository.findByUsername(user.getUsername());
    if (userFromDb == null) {
      throw new NoUserWithUsernameException(
          "No user found with the username: " + user.getUsername());
    }
    if (!userFromDb.getPassword().equals(user.getPassword())) {
      throw new IncorrectPasswordException("Invalid password", user);
    }
    return new UserJWT(userFromDb);
  }
}
