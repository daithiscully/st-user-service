package com.scully.userservice.controllers;

import com.scully.userservice.domain.users.User;
import com.scully.userservice.domain.users.UserJWT;
import com.scully.userservice.domain.users.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

  private static final Logger logger = LoggerFactory.getLogger(UserController.class);

  private final UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  @PostMapping
  public ResponseEntity<User> createNewUser(@RequestBody User user) {
    logger.info("Saving User: " + user);
    return ResponseEntity.ok(userService.saveNewUser(user));
  }

  @GetMapping("/{id}")
  public ResponseEntity<User> getUserById(@PathVariable Long id) {
    logger.info("Fetching User with id: " + id);
    return ResponseEntity.ok(userService.getUserById(id));
  }

  @PostMapping("/login")
  public ResponseEntity<UserJWT> authenticate(@RequestBody User user) {
    UserJWT userJWT = userService.authenticateUser(user);
    return ResponseEntity.ok(userJWT);
  }


}
