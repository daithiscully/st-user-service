package com.scully.userservice.controllers;

import com.scully.userservice.domain.common.ApiErrorResponse;
import com.scully.userservice.domain.users.exceptions.IncorrectPasswordException;
import com.scully.userservice.domain.users.exceptions.JWTCreationException;
import com.scully.userservice.domain.users.exceptions.NoUserWithUsernameException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

  @ExceptionHandler(JWTCreationException.class)
  public ResponseEntity<ApiErrorResponse> errorCreatingJWT(JWTCreationException ex) {
    ApiErrorResponse response = new ApiErrorResponse(ex.getMessage());

    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(IncorrectPasswordException.class)
  public ResponseEntity<ApiErrorResponse> incorrectPassword(IncorrectPasswordException ex) {
    ApiErrorResponse response = new ApiErrorResponse(ex.getMessage());

    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(NoUserWithUsernameException.class)
  public ResponseEntity<ApiErrorResponse> noUserFoundWithUsername(NoUserWithUsernameException ex) {
    ApiErrorResponse response = new ApiErrorResponse(ex.getMessage());

    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
  }
}
